#include <iostream>
#include <iomanip>
#include <random>

using namespace std;

random_device rd;

int randint(int min, int max, random_device& rd) {
	mt19937_64 gen(rd());
	return gen() % (max - min) + min;
}

int* arreglo(int* n) {
	*n = randint(100, 501, rd);
	int* elementos = new int[*n];

	for (int i = 0; i < *n; ++i) {
		elementos[i] = randint(1, 10001, rd);
	}
	return elementos;
}

void printArray(int* arr, int n) {
	cout << "[";
	for (int i = 0; i < n; ++i) {
		cout << " " << arr[i];
	}
	cout << "]\n";
}

int main() {
	int* n = new int;
	int* elementos = arreglo(n);

	printArray(elementos, *n);

	delete n;
	delete[]elementos;
	system("pause");
	return EXIT_SUCCESS;
}